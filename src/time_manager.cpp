#include "time_manager.h"

namespace EasyRTS
{
   void TimeManager::initialise()
   {
      mStartTimePoint = std::chrono::steady_clock::now();
      mCurrentTimePoint = mStartTimePoint;
      updateTime();
   }

   void TimeManager::updateTime()
   {
      auto newTimePoint = std::chrono::steady_clock::now();
      std::chrono::duration<double> duration = newTimePoint - mCurrentTimePoint;
      mDeltaTime = duration.count();
      mTime += mDeltaTime;
      mCurrentTimePoint = newTimePoint;
   }

   float TimeManager::getTimeSeconds()
   {
      return mTime;
   }

   float TimeManager::getDeltaTimeSeconds()
   {
      return mDeltaTime;
   }
}
