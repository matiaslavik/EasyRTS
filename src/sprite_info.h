#pragma once

#include <string>
#include <unordered_map>
#include <glm/vec2.hpp>

namespace EasyRTS
{
   /*
   * Information about an animation (usually a single row) inside a spritesheet.
   **/
   class SpriteAnimInfo
   {
      /* Position in sprite sheet */
      glm::tvec2<int> mSpritesheetPos;
      /* Number of frames (columns) in animation */
      int mFrameCount;
   };

   /*
   * Information about a sprite sheet, containing animations.
   **/
   struct SpritesheetInfo
   {
   public:
      /* path/ID of sprite sheet file. */
      std::string mSpriteSheet;

      /* The width and height of a single sprite in the sprite sheet. */
      glm::tvec2<int> mSpriteSize;

      /* Maps animation state to animation (animation state = "walk", "idle", etc.) */
      std::unordered_map<std::string, SpriteAnimInfo> mAnimations;
   };
}
