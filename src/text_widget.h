#pragma once
#include "widget.h"
#include "text_visual.h"
#include <string>
#include "glm/vec2.hpp"

namespace EasyRTS
{
   class TextWidget : public Widget
   {
   private:
      TextVisual* mTextVisual;

   public:
      TextWidget();
      ~TextWidget();

      void setText(const std::string text);
      void setOrigin(const glm::vec2 origin);
   };
}
