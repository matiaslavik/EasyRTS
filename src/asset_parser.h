#pragma once
#include "building_info.h"
#include "unit_info.h"
#include "resource_info.h"
#include <string>
#include "spritesheet_info.h"
#include "picojson/picojson.h"
#include "glm/vec2.hpp"

namespace EasyRTS
{
   class AssetParser
   {
   private:
      static SpritesheetInfo* parseSpritesheet(picojson::value outerValue);
      static void parseAnimations(picojson::value outerValue, SpritesheetInfo* spritesheetInfo);
      static glm::vec2 parseVec2(picojson::value value);

   public:
      static BuildingInfo* parseBuilding(std::string path);
      static UnitInfo* parseUnit(std::string path);
      static ResourceInfo* parseResource(std::string path);
   };
}
