#pragma once

#include "building_info.h"
#include "entity.h"
#include "asset_manager.h"

namespace EasyRTS
{
   class Building : public Entity
   {
   private:
      BuildingInfo* mBuildingInfo;

   public:
      Building(BuildingInfo* buildingInfo, AssetManager* assetManager);

      virtual void setPosition(glm::vec2 pos) override;
   };
}
