#pragma once

namespace EasyRTS
{
   enum class VisualType
   {
      ImageVisual,
      TextVisual
   };

   class Visual
   {
   public:
      Visual() {}
      virtual ~Visual() {}

      virtual VisualType getType() = 0;
   };
}
