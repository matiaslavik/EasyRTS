#pragma once

#include <string>
#include "spritesheet_info.h"

namespace EasyRTS
{
   class BuildingInfo
   {
   public:
      std::string mBuildingName;
      int mHealth;
      SpritesheetInfo* mSpritesheet;
      std::string mThumbnail;
   };
}
