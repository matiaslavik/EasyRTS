#pragma once

#include "spritesheet_info.h"
#include <optional>
#include "SFML/Graphics.hpp"

namespace EasyRTS
{
   class SpriteAnimator
   {
   private:
      SpritesheetInfo* mSpritesheet;
      sf::Sprite* mSprite;
      std::optional<SpriteAnimInfo> mCurrentAnimation;
      float mTimeToTick;
      int mCurrentFrame;

      void updateSprite();

   public:
      SpriteAnimator(SpritesheetInfo* spritesheet, sf::Sprite* sprite);
      
      bool hasAnimation(std::string animName);
      void playAnimation(std::string animName);
      void tick(float deltaTime);
   };
}
