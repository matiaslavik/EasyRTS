#pragma once
#include "widget.h"
#include "image_visual.h"
#include <string>

namespace EasyRTS
{
   class ImageWidget : public Widget
   {
   private:
      ImageVisual* mImageVisual;

   public:
      ImageWidget();
      virtual ~ImageWidget();

      /** Sets the path to the image file to be rendered. */
      void setImagePath(const std::string imagePath);
   };
}
