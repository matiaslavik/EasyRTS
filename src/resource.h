#pragma once

#include "resource_info.h"
#include "entity.h"
#include "asset_manager.h"

namespace EasyRTS
{
   class Resource : public Entity
   {
   private:
      ResourceInfo* mResourceInfo;

   public:
      Resource(ResourceInfo* resourceInfo, AssetManager* assetManager);

      virtual void setPosition(glm::vec2 pos) override;
   };
}
