#include "sprite_animator.h"
#include <iostream>

namespace EasyRTS
{
   SpriteAnimator::SpriteAnimator(SpritesheetInfo* spritesheet, sf::Sprite* sprite)
   {
      mSpritesheet = spritesheet;
      mSprite = sprite;
      mCurrentAnimation = {};
      mTimeToTick = 0.0f;
      mCurrentFrame = 0;

      updateSprite();
   }

   void SpriteAnimator::updateSprite()
   {
      glm::tvec2<int> spritePos(0, 0);
      if(mCurrentAnimation.has_value())
         spritePos = mCurrentAnimation->mSpritesheetPos;
      spritePos.x += mSpritesheet->mSpriteSize.x * mCurrentFrame;
      mSprite->setTextureRect(sf::IntRect(spritePos.x, spritePos.y, mSpritesheet->mSpriteSize.x, mSpritesheet->mSpriteSize.y));
   }

   bool SpriteAnimator::hasAnimation(std::string animName)
   {
      return mSpritesheet->mAnimations.find(animName) != mSpritesheet->mAnimations.end();
   }

   void SpriteAnimator::playAnimation(std::string animName)
   {
      auto iter = mSpritesheet->mAnimations.find(animName);
      if (iter != mSpritesheet->mAnimations.end())
      {
         mCurrentAnimation = iter->second;
         mTimeToTick = 1.0f / mCurrentAnimation->mFPS;
      }
      else
         std::cout << "ERROR: Animation not found: " << animName << std::endl;
   }

   void SpriteAnimator::tick(float deltaTime)
   {
      if (mCurrentAnimation.has_value())
      {
         mTimeToTick -= deltaTime;;
         while (mTimeToTick <= 0.0f)
         {
            mTimeToTick += 1.0f / mCurrentAnimation->mFPS;
            mCurrentFrame = (mCurrentFrame + 1) % mCurrentAnimation->mFrameCount;

            updateSprite();
         }
      }
   }
}
