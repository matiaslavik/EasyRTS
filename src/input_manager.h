#pragma once
#include "SFML/Window/Event.hpp"
#include <set>
#include "glm/vec2.hpp"

namespace sf
{
   class RenderWindow;
}

namespace EasyRTS
{
   class InputManager
   {
   private:
      sf::RenderWindow* mRenderWindow;
      std::set<int> mMouseDownEvents;
      std::set<int> mMouseUpEvents;

   public:
      InputManager(sf::RenderWindow* renderWindow);

      void handleEvent(const sf::Event evt);
      void clearEvents();

      bool getMouseButtonDown(int mouseButtonID) const;
      bool getMouseButtonUp(int mouseButtonID) const;
      bool getMouseButton(int mouseButtonID) const;

      glm::ivec2 getMousePosition() const;
   };
}
