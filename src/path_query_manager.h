#pragma once
#include "path_query.h"
#include <vector>
#include <memory>

namespace EasyRTS
{
   class Scene;

   struct PathFinderNode
   {
      glm::ivec2 mTile;
      bool mBlocked; // TODO: Use uint32_t (bit flags for discovered by player 1, 2, 3 etc.)
      size_t mParentIndex;
   };

   class PathQueryManager
   {
   private:
      std::vector<PathFinderNode> mTileMap;
      Scene* mScene;

   public:
      PathQueryManager(Scene* scene);
      void findPath(std::shared_ptr<PathQuery> query);
   };
}
