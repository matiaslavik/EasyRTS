#include "text_widget.h"

namespace EasyRTS
{
   TextWidget::TextWidget()
   {
      mTextVisual = new TextVisual();
      addVisual(mTextVisual);
   }

   TextWidget::~TextWidget()
   {
      delete mTextVisual;
   }

   void TextWidget::setText(const std::string text)
   {
      mTextVisual->setText(text);
   }

   void TextWidget::setOrigin(const glm::vec2 origin)
   {
      mTextVisual->setOrigin(origin);
   }
}
