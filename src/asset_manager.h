#pragma once

#include <string>
#include <unordered_map>
#include "unit_info.h"
#include "resource_info.h"
#include "building_info.h"

namespace sf
{
   class Texture;
}

namespace EasyRTS
{
   class AssetManager
   {
   private:
      std::unordered_map<std::string, sf::Texture*> mTextures;
      std::unordered_map<std::string, UnitInfo*> mUnitInfos;
      std::unordered_map<std::string, BuildingInfo*> mBuildingInfos;
      std::unordered_map<std::string, ResourceInfo*> mResourceInfos;

   public:
      void storeTexture(std::string id, sf::Texture* texture);
      void storeUnitInfo(UnitInfo* unitInfo);
      void storeBuildingInfo(BuildingInfo* buildingInfo);
      void storeResourceInfo(ResourceInfo* resourceInfo);

      sf::Texture* getTexture(std::string path);
      UnitInfo* getUnitInfo(std::string id);
      BuildingInfo* getBuildingInfo(std::string id);
      ResourceInfo* getResourceInfo(std::string id);

      void LoadAssets();
   };
}
