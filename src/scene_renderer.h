#pragma once
#include "scene.h"
#include <vector>
#include "SFML/Graphics.hpp"
#include "tile_highligher.h"

namespace EasyRTS
{
   class SceneRenderer
   {
   private:
      Scene* mScene;
      sf::RenderWindow* mRenderWindow;

   public:
      SceneRenderer(Scene* scene, sf::RenderWindow* renderWindow);

      void renderScene(float deltaTime);
   };
}
