#include "path_query_manager.h"
#include "scene.h"
#include <queue>

namespace EasyRTS
{
   PathQueryManager::PathQueryManager(Scene* scene)
   {
      mScene = scene;
   }

   void PathQueryManager::findPath(std::shared_ptr<PathQuery> query)
   {
      glm::ivec2 targetTile = query->mTargetTile;

      glm::ivec2 mapDimension = mScene->getMapSize();
      auto getTileIndex = [&](const int x, const int y)
      {
         return x + y * mapDimension.x;
      };

      std::vector<PathFinderNode> nodes;
      for (int y = 0; y < mapDimension.y; ++y)
      {
         for (int x = 0; x < mapDimension.x; ++x)
         {
            PathFinderNode node;
            node.mTile = glm::ivec2(x, y);
            node.mBlocked = false;
            node.mParentIndex = 0;
            nodes.push_back(node);
         }
      }

      if (nodes[getTileIndex(targetTile.x, targetTile.y)].mBlocked)
         return; // Target tile is blocked!

      std::vector<EntityRef>& entities = mScene->getEntities();
      for(EntityRef& entityRef : entities) // TODO: parallel std::for_each?
      {
         if (entityRef.isBuilding() || entityRef.isResource())
         {
            auto entity = entityRef.getEntity();
            glm::ivec2 entityPos(entity->mCartesianPos);
            for (int y = 0; y < entity->mSpritesheet->mOccupyTileCount.y; ++y)
            {
               for (int x = 0; x < entity->mSpritesheet->mOccupyTileCount.x; ++x)
               {
                  const glm::ivec2 pos = entityPos - glm::ivec2(x, y);
                  const int index = getTileIndex(pos.x, pos.y);
                  nodes[index].mBlocked = true;
               }
            }
         }
      }

      int resultIndex = -1;

      std::queue<glm::ivec2> tileQueue;
      std::set<int> visited;
      
      tileQueue.push(query->mStartTile);
      visited.insert(getTileIndex(query->mStartTile.x, query->mStartTile.y));

      auto pushTile = [&](glm::ivec2 tile, int parentIndex)
      {
         const int tileIndex = getTileIndex(tile.x, tile.y);
         if (visited.find(tileIndex) == visited.end() && !nodes[tileIndex].mBlocked)
         {
            tileQueue.push(tile);
            nodes[tileIndex].mParentIndex = parentIndex;
            visited.insert(tileIndex);
         }
      };

      while (!tileQueue.empty())
      {
         const glm::ivec2 currTile = tileQueue.front();
         tileQueue.pop();

         int currIndex = getTileIndex(currTile.x, currTile.y);

         // Check if we have reached the target
         if (currTile == targetTile)
         {
            resultIndex = currIndex;
            break;
         }

         // Add adjacent tiles
         if(currTile.x + 1 < mapDimension.x)
            pushTile(currTile + glm::ivec2(1, 0), currIndex);
         if (currTile.x > 0)
            pushTile(currTile - glm::ivec2(1, 0), currIndex);
         if (currTile.y + 1 < mapDimension.y)
            pushTile(currTile + glm::ivec2(0, 1), currIndex);
         if (currTile.y > 0)
            pushTile(currTile - glm::ivec2(0, 1), currIndex);
         if (currTile.x + 1 < mapDimension.x && currTile.y + 1 < mapDimension.y)
            pushTile(currTile + glm::ivec2(1, 1), currIndex);
         if (currTile.x + 1 < mapDimension.x && currTile.y > 0)
            pushTile(currTile + glm::ivec2(1, -1), currIndex);
         if (currTile.x > 0 && currTile.y + 1 < mapDimension.y)
            pushTile(currTile + glm::ivec2(-1, 1), currIndex);
         if (currTile.x > 0 && currTile.y > 0)
            pushTile(currTile + glm::ivec2(-1, -1), currIndex);
      }

      if (resultIndex == -1)
         return;

      PathFinderNode currNode = nodes[resultIndex];
      while (currNode.mParentIndex != 0)
      {
         query->mPath.push(currNode.mTile);
         currNode = nodes[currNode.mParentIndex];
      }

      query->mResultReady = true;
   }
}
