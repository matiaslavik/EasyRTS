#pragma once

#include "unit_info.h"
#include "entity.h"
#include "asset_manager.h"

namespace EasyRTS
{
   class PathQuery;

   class Unit : public Entity
   {
   private:
      UnitInfo* mUnitInfo;

   public:
      Unit(UnitInfo* unitInfo, AssetManager* assetManager);

      void setMoveAnim(const glm::vec2 moveDir);
      void setIdleAnim();
   };
}
