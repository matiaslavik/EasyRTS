#pragma once
#include "glm/vec2.hpp"
#include <optional>

namespace EasyRTS
{
	enum class WidgetPositioningMode // TODO: Merge with WidgetScalingMode?
	{
		Absolute,
		Relative
	};

	enum class WidgetScalingMode // TODO: Merge with WidgetPositioningMode?
	{
		Absolute,
		Relative
	};

	struct WidgetRect
	{
		glm::vec2 mPosition;
		glm::vec2 mSize;
	};

	class WidgetTransform
	{
	public:
		glm::vec2 mPosition = glm::vec2(0.0f, 0.0f);
		glm::vec2 mSize = glm::vec2(1.0f, 1.0f);
		glm::vec2 mPivot = glm::vec2(0.0f, 0.0f);

		WidgetPositioningMode mVerticalPositioning = WidgetPositioningMode::Relative;
		WidgetPositioningMode mHorizontalPositioning = WidgetPositioningMode::Relative;

		WidgetScalingMode mVerticalScaling = WidgetScalingMode::Relative;
		WidgetScalingMode mHorizontalScaling = WidgetScalingMode::Relative;

	public:
		WidgetTransform();

		/**
		* Creates an absolute WidgetTransform from this (local) WidgetTransform.
		*/
		WidgetRect createAbsoluteWidgetRect(const std::optional<WidgetRect> parentRect);

	};
}
