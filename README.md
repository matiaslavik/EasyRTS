# OpenRTS Engine

<img src="https://files.mastodon.social/media_attachments/files/105/380/772/639/505/483/original/80742e6aa08c6284.png" width="500px">

# How to clone and build

__WINDOWS__
- Requirements: CMake 3.3 or later
- Clone the repository (recursively!): git clone --recursive https://codeberg.org/matiaslavik/EasyRTS
- Run CMake to generate the project
- Build and run

__LINUX__
- Requirements: CMake 3.3 or later
- Clone the repository (recursively!): git clone --recursive https://codeberg.org/matiaslavik/EasyRTS
- Run getdeps-linux.sh (or manually do apt-get on the dependencies). Note: if you get "permission denied", execute "chmod +x aptget.sh" in the terminal first.
- Run CMake to generate the project
- Build and run

__NOTE__: The "assets" folder needs to be in the same folder as the executable.
