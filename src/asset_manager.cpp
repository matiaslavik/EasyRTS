#include "asset_manager.h"
#include <iostream>
#include "asset_parser.h"
#include "SFML/Graphics.hpp"

namespace EasyRTS
{
   void AssetManager::storeTexture(std::string id, sf::Texture* texture)
   {
      mTextures[id] = texture;
   }

   void AssetManager::storeUnitInfo(UnitInfo* unitInfo)
   {
      mUnitInfos[unitInfo->mUnitName] = unitInfo;
   }

   void AssetManager::storeBuildingInfo(BuildingInfo* buildingInfo)
   {
      mBuildingInfos[buildingInfo->mBuildingName] = buildingInfo;
   }

   void AssetManager::storeResourceInfo(ResourceInfo* resourceInfo)
   {
      mResourceInfos[resourceInfo->mResourceName] = resourceInfo;
   }

   sf::Texture* AssetManager::getTexture(std::string path)
   {
      auto it = mTextures.find(path);
      if (it != mTextures.end())
         return it->second;
      else
      {
         sf::Texture* texture = new sf::Texture();
         if (texture->loadFromFile(path))
         {
            storeTexture(path, texture);
            return texture;
         }
         else
         {
            delete texture;
            return nullptr;
         }
      }
   }

   UnitInfo* AssetManager::getUnitInfo(std::string id)
   {
      auto it = mUnitInfos.find(id);
      if (it != mUnitInfos.end())
         return it->second;
      else
         return nullptr;
   }

   BuildingInfo* AssetManager::getBuildingInfo(std::string id)
   {
      auto it = mBuildingInfos.find(id);
      if (it != mBuildingInfos.end())
         return it->second;
      else
         return nullptr;
   }

   ResourceInfo* AssetManager::getResourceInfo(std::string id)
   {
      auto it = mResourceInfos.find(id);
      if (it != mResourceInfos.end())
         return it->second;
      else
         return nullptr;
   }

   void AssetManager::LoadAssets()
   {
      // TODO: Load assets on demand (assume asset ID is same as filename + .json)

      // Buildings
      storeBuildingInfo(AssetParser::parseBuilding("assets/house.json"));
      storeBuildingInfo(AssetParser::parseBuilding("assets/smith.json"));
      storeBuildingInfo(AssetParser::parseBuilding("assets/firestation.json"));

      // Units
      storeUnitInfo(AssetParser::parseUnit("assets/worker.json"));

      // Resources
      storeResourceInfo(AssetParser::parseResource("assets/stone.json"));
      storeResourceInfo(AssetParser::parseResource("assets/tree.json"));
   }
}
