#include "game_engine.h"
#include "unit_conversion.h"
#include "asset_parser.h"
#include "scene_renderer.h"
#include "widget_renderer.h"
#include "script_manager.h"
#include "resource.h"
#include "SFML/Window.hpp"
#include "input_manager.h"
#include "path_query_manager.h"
#include "unit_movement_manager.h"
#include <iostream>

namespace EasyRTS
{
   void GameEngine::initialise()
   {
      mRenderWindow = new sf::RenderWindow(sf::VideoMode(1024, 768), "OpenRTSEngine");

      mAssetManager = new AssetManager();
      mAssetManager->LoadAssets();

      mTimeManager = new TimeManager();
      mTimeManager->initialise();
      
      mView = new sf::View(sf::FloatRect(-450.0f, -200.0f, 1024.f, 768));

      mScene = new Scene(mAssetManager);
      mSceneRenderer = new SceneRenderer(mScene, mRenderWindow);

      mInputManager = new InputManager(mRenderWindow);

      mWidgetRenderer = new WidgetRenderer(mRenderWindow, mAssetManager);
     
      mPathQueryManager = new PathQueryManager(mScene);
      mUnitMovementManager = new UnitMovementManager(mScene);

      mScriptManager = new ScriptManager(this);
      mScriptManager->invokeStartupHandler();

      // Set initial mouse position to window centre
      sf::Mouse::setPosition(sf::Vector2i(mRenderWindow->getPosition()) + sf::Vector2i(mRenderWindow->getSize().x / 2, mRenderWindow->getSize().y / 2));
   }

   void GameEngine::update()
   {
      if (mRenderWindow->isOpen())
      {
         mTimeManager->updateTime();

         mInputManager->clearEvents();

         mScene->tickScene(mTimeManager->getDeltaTimeSeconds());
         mUnitMovementManager->updateUnits(mTimeManager->getDeltaTimeSeconds());

         sf::Event event;
         while (mRenderWindow->pollEvent(event))
         {
            mInputManager->handleEvent(event);

            if (event.type == sf::Event::Closed)
            {
               mRenderWindow->close();
               return; // TODO
            }
         }

         handeMouseScroll();

         mScriptManager->invokeUpdate(mTimeManager->getDeltaTimeSeconds());

         mRenderWindow->clear(sf::Color::Black);

         mRenderWindow->setView(*mView);
         mSceneRenderer->renderScene(mTimeManager->getDeltaTimeSeconds());

         mRenderWindow->setView(mRenderWindow->getDefaultView());
         mWidgetRenderer->renderWidgets();

         mRenderWindow->setView(*mView);

         mRenderWindow->display();
      }
   }

   glm::vec2 GameEngine::screenToWorld(glm::ivec2 screenPos) const
   {
      return UnitConversion::isometricToCartesian(glm::vec2(screenPos.x - 32.0f, screenPos.y - 32.0f) / glm::vec2(32.0f, 32.0f));
   }

   void GameEngine::handeMouseScroll()
   {
      if (mRenderWindow->hasFocus())
      {
         sf::Vector2i mousePos = sf::Mouse::getPosition();
         sf::Vector2i minMousePos = mRenderWindow->getPosition();
         sf::Vector2i maxMousePos = sf::Vector2i(mRenderWindow->getSize()) + minMousePos;

         sf::Vector2f viewCentre = mView->getCenter();

         const float scrollSpeed = 1000.0f * mTimeManager->getDeltaTimeSeconds();

         if (mousePos.x >= maxMousePos.x)
            viewCentre.x += scrollSpeed;
         if (mousePos.x <= minMousePos.x)
            viewCentre.x -= scrollSpeed;
         if (mousePos.y >= maxMousePos.y)
            viewCentre.y += scrollSpeed;
         if (mousePos.y <= minMousePos.y)
            viewCentre.y -= scrollSpeed;

         mView->setCenter(viewCentre);

         mousePos.x = std::clamp(mousePos.x, minMousePos.x, static_cast<int>(maxMousePos.x));
         mousePos.y = std::clamp(mousePos.y, minMousePos.y, static_cast<int>(maxMousePos.y));

         sf::Mouse::setPosition(mousePos);
      }
   }
}
