#include "widget_transform.h"

namespace EasyRTS
{
	WidgetTransform::WidgetTransform()
	{
		mPosition = glm::vec2(0.0f, 0.0f);
		mSize = glm::vec2(0.0f, 0.0f);
		mPivot = glm::vec2(0.0f, 0.0f);

		mVerticalPositioning = WidgetPositioningMode::Relative;
		mHorizontalPositioning = WidgetPositioningMode::Relative;

		mVerticalScaling = WidgetScalingMode::Relative;
		mHorizontalScaling = WidgetScalingMode::Relative;

	}

	WidgetRect WidgetTransform::createAbsoluteWidgetRect(const std::optional<WidgetRect> parentRect)
	{
		WidgetRect absoluteRect;
		absoluteRect.mPosition = mPosition;
		absoluteRect.mSize = mSize;

		// Am I the transform of a root widget
		if (!parentRect.has_value())
		{
			absoluteRect.mPosition = mPosition;
			absoluteRect.mSize = mSize;
			return absoluteRect;
		}

		const glm::vec2 parentPos = parentRect->mPosition;
		const glm::vec2 parentSize = parentRect->mSize;

		if (mHorizontalPositioning == WidgetPositioningMode::Relative)
		{
			absoluteRect.mPosition.x = (parentPos.x + parentSize.x) * mPosition.x + parentPos.x * (1.0f - mPosition.x);
		}
		if (mVerticalPositioning == WidgetPositioningMode::Relative)
		{
			absoluteRect.mPosition.y = (parentPos.y + parentSize.y) * mPosition.y + parentPos.y * (1.0f - mPosition.y);
		}
		if (mHorizontalScaling == WidgetScalingMode::Relative)
		{
			absoluteRect.mSize.x = parentSize.x * mSize.x;
		}
		if (mVerticalScaling == WidgetScalingMode::Relative)
		{
			absoluteRect.mSize.y = parentSize.y * mSize.y;
		}

		const glm::vec2 absPivotPos = (absoluteRect.mPosition + absoluteRect.mSize) * mPivot + absoluteRect.mPosition * (1.0f - mPivot);
		absoluteRect.mPosition -= (absPivotPos - absoluteRect.mPosition);

		return absoluteRect;
	}

}