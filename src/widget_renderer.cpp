#include "widget_renderer.h"
#include "SFML/Graphics.hpp"
#include <cmath>

namespace EasyRTS
{
   WidgetRenderer::WidgetRenderer(sf::RenderWindow* renderWindow, AssetManager* assetManager)
   {
      mRenderWindow = renderWindow;
      mAssetManager = assetManager;
      mRootWidget = new Widget();
      mRootWidget->setHorizontalPositioning(WidgetPositioningMode::Absolute);
      mRootWidget->setVerticalPositioning(WidgetPositioningMode::Absolute);
      mRootWidget->setHorizontalScaling(WidgetScalingMode::Absolute);
      mRootWidget->setVerticalScaling(WidgetScalingMode::Absolute);
      mRootWidget->setSize(renderWindow->getSize().x, renderWindow->getSize().y);
   }

   void WidgetRenderer::renderImageVisual(ImageVisual* imageVisual, const WidgetRenderParams renderParams)
   {
      // Create sprite if it's null
      // TODO: Make sure this doesn't get called every frame if texture doesn't exist.
      // TODO: Also do this if sprite path changed.
      if (imageVisual->mSprite == nullptr && imageVisual->mImagePath != "")
      {
         sf::Texture* texture = mAssetManager->getTexture(imageVisual->mImagePath);
         if (texture != nullptr)
         {
            imageVisual->mSprite = new sf::Sprite(*texture);
         }
      }

      if (imageVisual->mSprite != nullptr)
      {
         if (renderParams.mVisualsNeedUpdate)
         {
            auto texRect = imageVisual->mSprite->getTextureRect();

            imageVisual->mSprite->setPosition(renderParams.mVisibleRect.mPosition.x, renderParams.mVisibleRect.mPosition.y);
            imageVisual->mSprite->setScale(renderParams.mVisibleRect.mSize.x / texRect.width, renderParams.mVisibleRect.mSize.y / texRect.height);
         }

         mRenderWindow->draw(*imageVisual->mSprite);
      }
   }

   void WidgetRenderer::renderTextVisual(TextVisual* textVisual, const WidgetRenderParams renderParams)
   {
      if (renderParams.mVisualsNeedUpdate)
      {
         textVisual->mText.setPosition(renderParams.mVisibleRect.mPosition.x, renderParams.mVisibleRect.mPosition.y);
      }

      mRenderWindow->draw(textVisual->mText);
   }

   void WidgetRenderer::renderWidget(Widget* widget, WidgetRenderParams renderParams)
   {
      renderParams.mVisualsNeedUpdate |= widget->mVisualsNeedUpdate;
      widget->mVisualsNeedUpdate = false;

      WidgetRect widgetRect = widget->getAbsoluteRect();
      WidgetRect parentRect = renderParams.mContentRect;

      // TODO: USE INT CALCULATION
      // Calculate content rect (rect to render widget in) and visible rect (usually the same, unless parent widget is smaller)
      const glm::vec2 contentXYBounds = widgetRect.mPosition + widgetRect.mSize;
      const glm::vec2 parentXYBounds = parentRect.mPosition + parentRect.mSize;
      const float visiblePosX = std::fminf(std::fmaxf(widgetRect.mPosition.x, parentRect.mPosition.x), parentXYBounds.x);
      const float visiblePosY = std::fminf(std::fmaxf(widgetRect.mPosition.y, parentRect.mPosition.y), parentXYBounds.y);
      const float visibleSizeW = std::fminf(contentXYBounds.x, parentXYBounds.x) - visiblePosX;
      const float visibleSizeH = std::fminf(contentXYBounds.y, parentXYBounds.y) - visiblePosY;
      
      // Update contect rect
      renderParams.mContentRect.mPosition = widgetRect.mPosition;
      renderParams.mContentRect.mSize = widgetRect.mSize;
      // Update visible rect
      renderParams.mVisibleRect.mPosition = glm::vec2(visiblePosX, visiblePosY);
      renderParams.mVisibleRect.mSize = glm::vec2(visibleSizeW, visibleSizeH);

      // Render visuals
      for (Visual* visual : widget->mVisuals)
      {
         VisualType type = visual->getType();
         if(type == VisualType::ImageVisual)
            renderImageVisual(static_cast<ImageVisual*>(visual), renderParams);
         else if (type == VisualType::TextVisual)
            renderTextVisual(static_cast<TextVisual*>(visual), renderParams);
      }

      // Render child widgets
      for (Widget* childWidget : widget->mChildWidgets)
      {
         renderWidget(childWidget, renderParams);
      }
   }

   void WidgetRenderer::renderWidgets()
   {
      for (Widget* widget : mRootWidget->mChildWidgets)
      {
         WidgetRenderParams renderParams;
         WidgetRect rootWidgetRect = mRootWidget->getAbsoluteRect();
         renderParams.mContentRect.mPosition = rootWidgetRect.mPosition;
         renderParams.mContentRect.mSize = rootWidgetRect.mSize;
         renderParams.mVisibleRect = renderParams.mContentRect;

         renderWidget(widget, renderParams);
      }
   }

   void WidgetRenderer::addWidget(Widget* widget)
   {
      mRootWidget->addWidget(widget);
   }
}
