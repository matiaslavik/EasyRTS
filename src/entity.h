#pragma once

#include "glm/vec2.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Graphics/Rect.hpp"
#include "sprite_animator.h"
#include <cstdint>

namespace EasyRTS
{
   class Entity // TODO: Remove this base class, and use a renderable interface instead?
   {
      static uint64_t GUID;

   public:
      glm::vec2 mCartesianPos = glm::vec2(0.0f, 0.0f);
      glm::vec2 mIsometricPos = glm::vec2(0.0f, 0.0f);
      sf::Sprite* mSprite = nullptr;
      SpriteAnimator* mSpriteAnimator = nullptr;
      SpritesheetInfo* mSpritesheet = nullptr;
      sf::FloatRect mGlobalBounds;
      uint64_t mGUID;

      Entity();
      ~Entity();

      virtual void setPosition(glm::vec2 pos);
   };
}
