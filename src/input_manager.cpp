#include "input_manager.h"
#include "SFML/Window/Mouse.hpp"
#include "SFML/Graphics/RenderWindow.hpp"

namespace EasyRTS
{
   InputManager::InputManager(sf::RenderWindow* renderWindow)
   {
      mRenderWindow = renderWindow;
   }

   void InputManager::handleEvent(const sf::Event evt)
   {
      switch (evt.type)
      {
      case sf::Event::MouseButtonPressed:
         mMouseDownEvents.insert(static_cast<int>(evt.mouseButton.button));
         break;
      case sf::Event::MouseButtonReleased:
         mMouseUpEvents.insert(static_cast<int>(evt.mouseButton.button));
         break;
      }
   }

   void InputManager::clearEvents()
   {
      mMouseDownEvents.clear();
      mMouseUpEvents.clear();
   }

   bool InputManager::getMouseButtonDown(int mouseButtonID) const
   {
      return mMouseDownEvents.find(mouseButtonID) != mMouseDownEvents.end();
   }

   bool InputManager::getMouseButtonUp(int mouseButtonID) const
   {
      return mMouseUpEvents.find(mouseButtonID) != mMouseUpEvents.end();
   }

   bool InputManager::getMouseButton(int mouseButtonID) const
   {
      return sf::Mouse::isButtonPressed(static_cast<sf::Mouse::Button>(mouseButtonID));
   }

   glm::ivec2 InputManager::getMousePosition() const
   {
      sf::Vector2i pos = sf::Mouse::getPosition() - mRenderWindow->getPosition();
      sf::Vector2f coords = mRenderWindow->mapPixelToCoords(pos, mRenderWindow->getView());
      return glm::ivec2(coords.x, coords.y);
   }
}
