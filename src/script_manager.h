#pragma once
#include <set>
#include <string>

namespace chaiscript
{
   class ChaiScript;
   class Boxed_Value;
}

namespace EasyRTS
{
   class GameEngine;

   class ScriptManager
   {
   private:
      GameEngine* mGameEngine;
      chaiscript::ChaiScript* mChaiScript;
      std::set<std::string> mRegisteredScripts;
      chaiscript::Boxed_Value* mGameStartupHandler;

      void addScriptBindings();

   public:
      ScriptManager(GameEngine* engine);

      void registerScript(const std::string scriptPath);
      void invokeStartupHandler();
      void invokeUpdate(float deltaTime);
   };
}
