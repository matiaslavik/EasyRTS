#include "widget.h"

namespace EasyRTS
{
   Widget::Widget()
   {
   }

   Widget::~Widget()
   {
      for (Widget* child : mChildWidgets)
         delete child;
   }

   void Widget::setTransformDirty()
   {
      mTransformIsDirty = true;
		mVisualsNeedUpdate = true;

		for (Widget* child : mChildWidgets)
			child->setTransformDirty();
   }

	void Widget::addVisual(Visual* visual)
	{
		mVisuals.push_back(visual);
		mVisualsNeedUpdate = true;
	}

   void Widget::addWidget(Widget* widget)
   {
      mChildWidgets.push_back(widget);
      widget->mParentWidget = this;
   }

   WidgetRect Widget::getAbsoluteRect()
   {
      if (mTransformIsDirty)
      {
         if(mParentWidget != nullptr)
            mAbsoluteRect = mTransform.createAbsoluteWidgetRect(mParentWidget->getAbsoluteRect());
         else
            mAbsoluteRect = mTransform.createAbsoluteWidgetRect(std::nullopt);

         mTransformIsDirty = false;
      }

      return mAbsoluteRect; // return the re-calculated absolute rect of the widget
   }

	void Widget::setPosition(glm::vec2 arg_pos)
	{
		mTransform.mPosition = arg_pos;
		setTransformDirty();
	}

	void Widget::setSize(glm::vec2 arg_size)
	{
		mTransform.mSize = arg_size;
		setTransformDirty();
	}

	void Widget::setPivot(glm::vec2 arg_pivot)
	{
		mTransform.mPivot = arg_pivot;
		setTransformDirty();
	}

	void Widget::setVerticalPositioning(WidgetPositioningMode arg_mode)
	{
		mTransform.mVerticalPositioning = arg_mode;
		setTransformDirty();
	}

	void Widget::setHorizontalPositioning(WidgetPositioningMode arg_mode)
	{
		mTransform.mHorizontalPositioning = arg_mode;
		setTransformDirty();
	}

	void Widget::setVerticalScaling(WidgetScalingMode arg_mode)
	{
		mTransform.mVerticalScaling = arg_mode;
		setTransformDirty();
	}

	void Widget::setHorizontalScaling(WidgetScalingMode arg_mode)
	{
		mTransform.mHorizontalScaling = arg_mode;
		setTransformDirty();
	}
}
